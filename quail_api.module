<?php

/**
 * @file
 * Module file for the quail api.
 */

/**
 * Implements hook_theme().
 */
function quail_api_theme($existing, $type, $theme, $path) {
  $themes = array();

  $themes['quail_api_results'] = array(
    'template' => 'quail_api_results',
    'variables' => array(
      'quail_severity_id' => NULL,
      'quail_severity_array' => NULL,
      'quail_severity_results' => array(),
      'quail_markup_format' => NULL,
      'quail_title_block' => 'h3',
      'quail_display_title' => TRUE,
      'quail_display_description' => TRUE,
      'quail_renderred_tests' => '',
    ),
  );

  $themes['quail_api_test'] = array(
    'template' => 'quail_api_test',
    'variables' => array(
      'quail_test_name' => '',
      'quail_test_results' => array(),
      'quail_test_note' => '',
      'quail_markup_format' => NULL,
      'quail_renderred_problems' => '',
    ),
  );

  $themes['quail_api_problem'] = array(
    'template' => 'quail_api_problem',
    'variables' => array(
      'quail_problem_id' => '',
      'quail_problem_data' => array(),
      'quail_markup_format' => NULL,
    ),
  );

  return $themes;
}

/**
 * Template preprocess function for quail_api_results.tpl.php.
 */
function template_preprocess_quail_api_results(&$variables) {
  $root_class_name = 'quail_api-results';

  if (!isset($variables['quail_severity_id'])) {
    $variables['quail_severity_id'] = NULL;
  }

  if (!isset($variables['quail_severity_machine_name'])) {
    $variables['quail_severity_machine_name'] = '';
  }

  if (!isset($variables['quail_severity_human_name'])) {
    $variables['quail_severity_human_name'] = '';
  }

  if (!isset($variables['quail_severity_description'])) {
    $variables['quail_severity_description'] = '';
  }

  if (!isset($variables['quail_base_class'])) {
    $variables['quail_base_class'] = $root_class_name;
  }

  if (!isset($variables['quail_specific_class'])) {
    $variables['quail_specific_class'] = 'unknown';
  }

  if (!isset($variables['quail_renderred_tests'])) {
    $variables['quail_renderred_tests'] = '';
  }

  if (empty($variables['quail_severity_results']) || !isset($variables['quail_severity_results']['total'])) {
    $quail_severity_results = array('total' => 0);
  }

  if (is_numeric($variables['quail_severity_id'])) {
    if (empty($variables['quail_severity_array'])) {
      $severity = \Drupal\quail_api\QuailApiSettings::get_severity($variables['quail_severity_id']);
    }
    else {
      $severity = $variables['quail_severity_array'];
    }

    $variables['quail_severity_id'] = $severity['id'];
    $variables['quail_severity_machine_name'] = $severity['machine_name'];
    $variables['quail_severity_human_name'] = $severity['human_name'];
    $variables['quail_severity_description'] = $severity['description'];
    $variables['quail_specific_class'] = $root_class_name . '-' . $severity['machine_name'];
  }
  unset($variables['quail_severity_array']);

  // limit quail_title_block to a specific list of html tags
  if (!in_array($variables['quail_title_block'], array('h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'span', 'p'))) {
    $variables['quail_title_block'] = 'h3';
  }

  $fallback_filter_format = \Drupal::config('quail_api.settings')->get('filter_format');
  if (empty($fallback_filter_format)) {
    $fallback_filter_format = filter_fallback_format();
  }

  $quail_renderred_tests = [];
  foreach ($variables['quail_severity_results'] as $test_name => $test_results) {
    if ($test_name === 'total') continue;

    $quail_renderred_tests[] = [
      '#theme' => 'quail_api_test',
      '#quail_test_name' => $test_name,
      '#quail_test_results' => $test_results,
      '#quail_markup_format' => is_null($variables['quail_markup_format']) ? $fallback_filter_format : $variables['quail_markup_format'],
    ];
  }

  if (!empty($quail_renderred_tests)) {
    $variables['quail_renderred_tests'] = \Drupal::service('renderer')->render($quail_renderred_tests, FALSE);
  }
}

/**
 * Template preprocess function for quail_api_test.tpl.php.
 */
function template_preprocess_quail_api_test(&$variables) {
  $root_class_name = 'quail_api-test';

  if (!isset($variables['quail_base_class'])) {
    $variables['quail_base_class'] = $root_class_name;
  }

  if (!isset($variables['quail_specific_class'])) {
    $variables['quail_specific_class'] = '';
  }

  if (!isset($variables['quail_test_title'])) {
    $variables['quail_test_title'] = '';
  }

  if (!isset($variables['quail_test_description'])) {
    $variables['quail_test_description'] = '';
  }

  if (!isset($variables['quail_test_problems'])) {
    $variables['quail_test_problems'] = array();
  }

  if (!empty($variables['quail_test_name'])) {
    $variables['quail_specific_class'] = $root_class_name . '-' . Drupal\Component\Utility\Unicode::strtolower(\Drupal\Component\Utility\Html::escape($variables['quail_test_name']));
  }

  $fallback_filter_format = filter_fallback_format();

  if (!empty($variables['quail_test_results']['body']['title'])) {
    $variables['quail_test_title'] = $variables['quail_test_results']['body']['title'];

    if (isset($variables['quail_test_results']['body']['description'])) {
      // drupal 8.4 and earlier seem to have issues with NULL being passed as the markup format.
      if (is_null($variables['quail_markup_format'])) {
        $variables['quail_test_description'] = check_markup($variables['quail_test_results']['body']['description'], $fallback_filter_format);
      }
      else {
        $variables['quail_test_description'] = check_markup($variables['quail_test_results']['body']['description'], $variables['quail_markup_format']);
      }
    }

    if (!empty($variables['quail_test_results']['problems'])) {
      $variables['quail_test_problems'] = $variables['quail_test_results']['problems'];
    }
  }

  $variables['quail_test_note'] = '';
  if (isset($variables['quail_test_problems']['message'])) {
    $variables['quail_test_note'] = $variables['quail_test_problems']['message'];
    unset($variables['quail_test_problems']['message']);
  }

  unset($variables['quail_test_name']);
  unset($variables['quail_test_results']);

  $renderred_problems = array();
  foreach ($variables['quail_test_problems'] as $problem_id => $problem_data) {
    if (!is_numeric($problem_id)) {
      continue;
    }

    $renderred_problems[] = array(
      '#theme' => 'quail_api_problem',
      '#quail_problem_id' => $problem_id,
      '#quail_problem_data' => $problem_data,
      '#quail_markup_format' => is_null($variables['quail_markup_format']) ? $fallback_filter_format : $variables['quail_markup_format'],
    );
  }

  $variables['quail_renderred_problems'] = '';
  if (!empty($renderred_problems)) {
    // this is the expanded version of the deprecated drupal_render() function
    // (consider RendererInterface::render()).
    $variables['quail_renderred_problems'] = \Drupal::service('renderer')->render($renderred_problems, FALSE);
  }
}

/**
 * Template preprocess function for quail_api_problem.tpl.php.
 */
function template_preprocess_quail_api_problem(&$variables) {
  $root_class_name = 'quail_api-problem';

  if (!isset($variables['quail_base_class'])) {
    $variables['quail_base_class'] = $root_class_name;
  }

  if (!isset($variables['quail_specific_class'])) {
    $variables['quail_specific_class'] = '';
  }

  if (!isset($variables['quail_problem_line'])) {
    $variables['quail_problem_line'] = '';
  }

  if (!isset($variables['quail_problem_description'])) {
    $variables['quail_problem_description'] = '';
  }

  if (isset($variables['quail_problem_id'])) {
    $variables['quail_specific_class'] = $root_class_name . '-' . Drupal\Component\Utility\Unicode::strtolower(\Drupal\Component\Utility\Html::escape($variables['quail_problem_id']));
  }

  if (isset($variables['quail_problem_data']['line'])) {
    $variables['quail_problem_line'] = '' . $variables['quail_problem_data']['line'];
  }

  $fallback_filter_format = filter_fallback_format();

  if (isset($variables['quail_problem_data']['element'])) {
    // drupal 8.4 and earlier seem to have issues with NULL being passed as the markup format.
    if (is_null($variables['quail_markup_format'])) {
      $variables['quail_problem_description'] = check_markup($variables['quail_problem_data']['element'], $fallback_filter_format);
    }
    else {
      $variables['quail_problem_description'] = check_markup($variables['quail_problem_data']['element'], $variables['quail_markup_format']);
    }
  }

  unset($variables['quail_problem_id']);
  unset($variables['quail_problem_data']);
}
